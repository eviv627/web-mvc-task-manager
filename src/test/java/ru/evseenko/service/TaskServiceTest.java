package ru.evseenko.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.evseenko.api.service.IProjectService;
import ru.evseenko.api.service.ITaskService;
import ru.evseenko.api.service.IUserService;
import ru.evseenko.config.ApplicationConfig;
import ru.evseenko.entity.dto.ProjectDTO;
import ru.evseenko.entity.dto.TaskDTO;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.util.DateFormatUtil;
import ru.evseenko.util.Sort;

import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= ApplicationConfig.class)
public class TaskServiceTest {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    private String testUserId;

    @Before
    public void init() {
        UserDTO testUser = new UserDTO();
        testUser.setId(UUID.randomUUID().toString());
        testUser.setLogin(generateUserName());
        testUser.setPasswordHash("hash");
        userService.persist(testUser);

        testUserId = testUser.getId();
    }

    @After
    public void removeUser() {
        userService.remove(testUserId);
    }

    @Test
    public void taskCreateTest() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setId(UUID.randomUUID().toString());
        task.setName("task1");
        task.setUserId(testUserId);
        task.setDescription("desc");
        task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
        task.setStartDate(DateFormatUtil.parseIsoDate(new Date()));
        task.setEndDate(DateFormatUtil.parseIsoDate(new Date()));

        taskService.persist(task.getUserId(), task);

        @NotNull final TaskDTO actualTask =
                taskService.get(task.getUserId(), task.getId());

        Assert.assertEquals(task.toString(), actualTask.toString());
    }

    @Test
    public void projectUpdateTest() {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setId(UUID.randomUUID().toString());
        task.setName("Task 1");
        task.setUserId(testUserId);
        task.setDescription("desc");
        task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
        task.setStartDate(DateFormatUtil.parseIsoDate(new Date()));
        task.setEndDate(DateFormatUtil.parseIsoDate(new Date()));

        taskService.persist(task.getUserId(), task);

        task.setName("upd_Task1");
        task.setDescription("upd_desc");
        task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
        task.setStartDate(DateFormatUtil.parseIsoDate(new Date()));
        task.setEndDate(DateFormatUtil.parseIsoDate(new Date()));

        taskService.update(task.getUserId(), task);

        @NotNull final TaskDTO actualTask =
                taskService.get(task.getUserId(), task.getId());

        Assert.assertEquals(task.toString(), actualTask.toString());
    }

    @Test
    public void taskDeleteAllTest() {
        final int tasksNum = 20;

        for (int i = 0; i < tasksNum; i++) {
            final TaskDTO task = new TaskDTO();
            task.setId(UUID.randomUUID().toString());
            task.setName("task" + i);
            task.setUserId(testUserId);
            task.setDescription("desc");
            task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
            taskService.persist(testUserId, task);
        }

        taskService.deleteAll(testUserId);

        Assert.assertEquals(new ArrayList<>(), taskService.getAll(testUserId));
    }

    @Test
    public void taskForProjectTest() {
        final int tasksNum = 200;

        @NotNull final List<TaskDTO> listTaskDTO = new ArrayList<>();
        final ProjectDTO project = new ProjectDTO();

        project.setId(UUID.randomUUID().toString());
        project.setName("project");
        project.setUserId(testUserId);
        project.setDescription("desc");
        project.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
        projectService.persist(testUserId, project);

        for (int i = 0; i < tasksNum; i++) {
            final TaskDTO task = new TaskDTO();
            task.setId(UUID.randomUUID().toString());
            task.setName("project" + i);
            task.setUserId(testUserId);
            task.setDescription("desc");
            task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
            task.setProjectId(((i % 10) > 5) ? project.getId() : "");
            taskService.persist(testUserId, task);

            if (!task.getProjectId().isEmpty()) {
                listTaskDTO.add(task);
            }
        }

        @NotNull final List<TaskDTO> actualTasks = taskService.getTaskForProject(testUserId, project.getId());
        actualTasks.sort(Sort.NAME.getComparatorDTO());
        listTaskDTO.sort(Sort.NAME.getComparatorDTO());

        Assert.assertEquals(listTaskDTO.toString(), actualTasks.toString());
    }

    @Test
    public void taskByNameTest() {
        final int tasksNum = 20;

        @NotNull final List<TaskDTO> listTaskDTO = new ArrayList<>();

        for (int i = 0; i < tasksNum; i++) {
            final TaskDTO task = new TaskDTO();
            task.setId(UUID.randomUUID().toString());
            task.setName("task" + i);
            task.setUserId(testUserId);
            task.setDescription("desc");
            task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
            taskService.persist(testUserId, task);

            if (task.getName().contains("task1")) {
                listTaskDTO.add(task);
            }
        }

        @NotNull final List<TaskDTO> actualTasks =
                taskService.findByNamePart(testUserId, "task1");
        actualTasks.sort(Sort.NAME.getComparatorDTO());
        listTaskDTO.sort(Sort.NAME.getComparatorDTO());

        Assert.assertEquals(listTaskDTO.toString(), actualTasks.toString());
    }

    @Test
    public void taskByDescriptionTest() {
        final int tasksNum = 20;

        @NotNull final List<TaskDTO> listTaskDTO = new ArrayList<>();

        for (int i = 0; i < tasksNum; i++) {
            final TaskDTO task = new TaskDTO();
            task.setId(UUID.randomUUID().toString());
            task.setName("task" + i);
            task.setUserId(testUserId);
            task.setDescription("desc" + i);
            task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
            taskService.persist(testUserId, task);

            if (task.getDescription().contains("desc1")) {
                listTaskDTO.add(task);
            }
        }

        @NotNull final List<TaskDTO> actualTasks =
                taskService.findByDescriptionPart(testUserId, "desc1");
        actualTasks.sort(Sort.NAME.getComparatorDTO());
        listTaskDTO.sort(Sort.NAME.getComparatorDTO());

        Assert.assertEquals(listTaskDTO.toString(), actualTasks.toString());
    }

    @NotNull
    private String generateUserName() {
        @NotNull final Random random = new Random();
        return "TestServiceUser" + random.nextInt(999999);
    }
}
