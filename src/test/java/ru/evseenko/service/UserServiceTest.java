package ru.evseenko.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.evseenko.api.service.IUserService;
import ru.evseenko.config.ApplicationConfig;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.dto.UserDTO;

import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= ApplicationConfig.class)
public class UserServiceTest {

    @Autowired
    private IUserService userService;

    private UserDTO user;

    @Before
    public void setUpTest() {
        user = new UserDTO();
        user.setId(UUID.randomUUID().toString());
        user.setLogin("LOGIN");
        user.setPasswordHash("password hash");
        user.setRole(Role.ADMIN);
        userService.persist(user);
    }

    @After
    public void tearDownTest() {
        userService.remove(user.getId());
    }

    @Test
    public void createUserTest() {
        @NotNull final UserDTO actualUser = userService.get(user.getId());
        Assert.assertEquals(user.toString(), actualUser.toString());
    }

    @Test
    public void updateUserTest() {
        user.setRole(Role.USER);
        user.setLogin("LOG");
        user.setPasswordHash("HASH");
        userService.update(user);

        @NotNull final UserDTO actualUser = userService.get(user.getId());
        Assert.assertEquals(user.toString(), actualUser.toString());
    }

    @Test
    public void getByLoginUserTest() {
        @Nullable final UserDTO actualUser = userService.getUserByLogin(user.getLogin());
        assert actualUser != null;
        Assert.assertEquals(user.toString(), actualUser.toString());
    }
}
