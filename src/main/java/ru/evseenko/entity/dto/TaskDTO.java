package ru.evseenko.entity.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.evseenko.api.entity.DomainDTO;
import ru.evseenko.entity.Status;

import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;

@Setter
@Getter
@XmlSeeAlso({Status.class})
public class TaskDTO implements Serializable, DomainDTO {

    public TaskDTO() {
        id = "";
        name = "";
        description = "";
        startDate = "";
        endDate = "";
        createDate = "";
        status = Status.PLANNED;
        userId = "";
        projectId = "";
    }

    @NotNull
    private String id;

    @NotNull
    private String projectId;

    @NotNull
    private String name;

    @NotNull
    private String description;

    @NotNull
    private String startDate;

    @NotNull
    private String endDate;

    @NotNull
    private String createDate;

    @NotNull
    private Status status;

    @NotNull
    private String userId;

    @Override
    public String toString() {
        return "TaskDTO {" +
                "id='" + id + '\'' +
                ", projectId='" + projectId + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", createDate=" + createDate +
                ", status=" + status +
                ", userId='" + userId + '\'' +
                '}';
    }
}
