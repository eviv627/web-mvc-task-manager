package ru.evseenko.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.evseenko.api.service.IProjectService;
import ru.evseenko.entity.Status;
import ru.evseenko.entity.dto.ProjectDTO;
import ru.evseenko.util.ControllerUtil;
import ru.evseenko.util.DateFormatUtil;

import java.util.*;

@Controller
@RequestMapping("/project")
public class ProjectController {
    private final IProjectService projectService;

    @Autowired
    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getProjects(final Model model) {
        List<ProjectDTO> projects = projectService.getAll(ControllerUtil.getCurrentUserId());
        model.addAttribute("projects", projects);
        return "project/project";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String getAddForm(final Model model) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(ControllerUtil.getCurrentUserId());
        model.addAttribute("project", project);
        return "project/add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addProject(
            @ModelAttribute("project") final ProjectDTO project,
            final Model model
    ) {
        project.setId(UUID.randomUUID().toString());
        project.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
        projectService.persist(project.getUserId(), project);
        return "redirect:/project/list";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteProject(
            @RequestParam(value="id") final String id,
            final Model model
    ) {
        @NotNull final ProjectDTO project = projectService.get(ControllerUtil.getCurrentUserId(), id);
        projectService.delete(project.getUserId(), project);
        return "redirect:/project/list";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String getEditForm(
            @RequestParam(value="id") final String id,
            final Model model
    ) {
        @NotNull final ProjectDTO project = projectService.get(ControllerUtil.getCurrentUserId(), id);
        model.addAttribute("project", project);
        return "project/edit";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String saveProject(
            @ModelAttribute("project") final ProjectDTO project,
            final Model model
    ) {
        projectService.update(project.getUserId(), project);
        return "redirect:/project/list";
    }

    @ModelAttribute
    public void setUpModel(final Model model) {
        model.addAttribute("statuses", Arrays.asList(Status.values()));
        model.addAttribute("datePattern", DateFormatUtil.ISO_DATE_REGEXP + "?");
    }
}
