package ru.evseenko.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.evseenko.api.service.ITaskService;
import ru.evseenko.entity.Status;
import ru.evseenko.entity.dto.TaskDTO;
import ru.evseenko.util.ConstantString;
import ru.evseenko.util.ControllerUtil;
import ru.evseenko.util.DateFormatUtil;

import java.util.*;

@Controller
@RequestMapping("/task")
public class TaskController {
    private final ITaskService taskService;

    @Autowired
    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getTasks(final Model model) {
        List<TaskDTO> tasks = taskService.getAll(ControllerUtil.getCurrentUserId());
        model.addAttribute("tasks", tasks);
        return "task/task";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String getAddForm(final Model model) {
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(ControllerUtil.getCurrentUserId());
        model.addAttribute("task", task);
        return "task/add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addTask(
            @ModelAttribute("task") final TaskDTO task,
            final Model model
    ) {
        task.setId(UUID.randomUUID().toString());
        task.setCreateDate(DateFormatUtil.parseIsoDate(new Date()));
        taskService.persist(task.getUserId(), task);
        return "redirect:/task/list";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteTask(
            @RequestParam(value="id") final String id,
            final Model model
    ) {
        @NotNull final TaskDTO task = taskService.get(ControllerUtil.getCurrentUserId(), id);
        taskService.delete(task.getUserId(), task);
        return "redirect:/task/list";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String getEditForm(
            @RequestParam(value="id") final String id,
            final Model model
    ) {
        @NotNull final TaskDTO task = taskService.get(ControllerUtil.getCurrentUserId(), id);
        model.addAttribute("task", task);
        return "task/edit";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String saveTask(
            @ModelAttribute("task") final TaskDTO task,
            final Model model
    ) {
        taskService.update(task.getUserId(), task);
        return "redirect:/task/list";
    }

    @ModelAttribute
    public void setUpModel(final Model model) {
        model.addAttribute("statuses", Arrays.asList(Status.values()));
        model.addAttribute("datePattern", DateFormatUtil.ISO_DATE_REGEXP + "?");
        model.addAttribute("projectIdPattern", ConstantString.UUID_REGEXP + "?");
    }
}
