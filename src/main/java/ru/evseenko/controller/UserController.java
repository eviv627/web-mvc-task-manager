package ru.evseenko.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.evseenko.api.security.ISecurityService;
import ru.evseenko.api.service.IUserService;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.dto.UserDTO;
import ru.evseenko.mvc.UserValidator;

@Controller
public class UserController {

    private final IUserService userService;
    private final ISecurityService securityService;
    private final UserValidator userValidator;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(
            IUserService userService,
            ISecurityService securityService,
            UserValidator userValidator,
            PasswordEncoder passwordEncoder
    ) {
        this.userService = userService;
        this.securityService = securityService;
        this.userValidator = userValidator;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/registration")
    public String registration(final Model model) {
        model.addAttribute("user", new UserDTO());
        return "user/registration";
    }

    @PostMapping("/registration")
    public String registration(
            @ModelAttribute("user") final UserDTO user,
            final BindingResult bindingResult,
            final Model model
    ) {
        userValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Your username or password is invalid.");
            return "user/registration";
        }

        user.setPasswordHash(passwordEncoder.encode(user.getPassword()));
        user.setRole(Role.ADMIN);
        user.setActive(true);
        userService.persist(user);
        securityService.autoLogin(user.getUsername(), user.getPasswordConfirm());

        return "redirect:/welcome";
    }

    @GetMapping("/login")
    public String login(
            final Model model,
            final String error,
            final String logout
    ) {
        if (error != null) {
            model.addAttribute("error", "Your username and password is invalid.");
        }

        if (logout != null) {
            model.addAttribute("message", "You have been logged out successfully.");
        }

        return "user/login";
    }

    @RequestMapping({"/", "/welcome"})
    public String welcome(final Model model) {
        return "index";
    }
}
