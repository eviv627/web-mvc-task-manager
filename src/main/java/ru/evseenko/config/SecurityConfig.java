package ru.evseenko.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import ru.evseenko.api.service.IUserService;
import ru.evseenko.mvc.SimpleAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages = { "ru.evseenko.security", "ru.evseenko.mvc" })
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final IUserService userDetailsService;

    private final SimpleAuthenticationSuccessHandler successHandler;

    @Qualifier("persistentTokenRepository")
    private final PersistentTokenRepository persistentTokenRepository;

    @Autowired
    SecurityConfig(
            SimpleAuthenticationSuccessHandler successHandler,
            IUserService userDetailsService,
            PersistentTokenRepository persistentTokenRepository
    ) {
        this.successHandler = successHandler;
        this.userDetailsService = userDetailsService;
        this.persistentTokenRepository = persistentTokenRepository;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/resources/**", "/registration", "/welcome", "/").permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/project", "/task").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .successHandler(successHandler)
                .permitAll()
                .and()
                .rememberMe()
                .rememberMeParameter("remember-me")
                .rememberMeCookieName("example-app-remember-me")
                .tokenRepository(persistentTokenRepository)
                .userDetailsService(userDetailsService)
                .tokenValiditySeconds(24 * 60 * 60)
                .and()
                .logout()
                .invalidateHttpSession(true)
                .clearAuthentication(true)
                .permitAll();
    }

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }
}
