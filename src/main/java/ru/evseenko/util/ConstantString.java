package ru.evseenko.util;

public class ConstantString {
    public static final String USER_ID_STUB = "8e3c718a-325e-40cd-aa81-bea9742b9c87";
    public static final String SESSION_USER_KEY = "SESSION_USER_KEY";
    public static final String SESSION_SIGNATURE_KEY = "SESSION_SIGNATURE_KEY";
    public static final String UUID_REGEXP = "(\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b)";
}