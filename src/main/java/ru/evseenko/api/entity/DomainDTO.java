package ru.evseenko.api.entity;

import ru.evseenko.entity.Status;

public interface DomainDTO extends Identifiable {

    String getName();

    void setName(String name);

    String getDescription();

    void setDescription(String description);

    String getStartDate();

    void setStartDate(String startDate);

    String getEndDate();

    void setEndDate(String endDate);

    String getCreateDate();

    void setCreateDate(String createDate);

    Status getStatus();

    void setStatus(Status status);

    String getUserId();

    void setUserId(String userId);
}
