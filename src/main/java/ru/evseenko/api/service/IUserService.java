package ru.evseenko.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.UserDetailsService;
import ru.evseenko.entity.dto.UserDTO;

public interface IUserService extends UserDetailsService {
    @Nullable
    UserDTO getUserByLogin(@NotNull String login);

    @NotNull
    UserDTO get(@NotNull String id);

    void update(@NotNull UserDTO entityDTO);

    void persist(@NotNull UserDTO entityDTO);

    void remove(@NotNull String id);
}
