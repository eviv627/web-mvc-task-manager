package ru.evseenko.api.service;

import org.jetbrains.annotations.NotNull;

import javax.servlet.http.HttpSession;

public interface ISessionService {
    void checkSession(@NotNull final HttpSession httpSession);

    void setUpSession(@NotNull final HttpSession httpSession, @NotNull final String userId);

    void tearDownSession(@NotNull final HttpSession httpSession);
}
