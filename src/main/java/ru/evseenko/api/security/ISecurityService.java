package ru.evseenko.api.security;

public interface ISecurityService {
    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
