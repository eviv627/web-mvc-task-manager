package ru.evseenko.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.evseenko.api.repository.IUserRepository;
import ru.evseenko.api.service.IUserService;
import ru.evseenko.entity.Role;
import ru.evseenko.entity.User;
import ru.evseenko.entity.dto.UserDTO;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserService implements IUserService, UserDetailsService {

    private final IUserRepository userRepository;

    @Autowired
    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    private IUserService self;

    @Nullable
    @Override
    @Transactional
    public UserDTO getUserByLogin(@NotNull final String login) {
        @Nullable final User user = userRepository.findUserByLogin(login);
        if (user == null) {
            return null;
        }
        @NotNull final UserDTO userDTO = produceDTO(user);
        return userDTO;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO get(@NotNull final String id) {
        @NotNull final Optional<User> optional = userRepository.findById(id);
        if(!optional.isPresent()) {
            throw new SecurityException("no such user");
        }
        @NotNull final User user = optional.get();
        @NotNull final UserDTO userDTO = produceDTO(user);

        return userDTO;
    }

    @Override
    @Transactional
    public void update(@NotNull final UserDTO entityDTO) {
        @NotNull final Optional<User> optional = userRepository.findById(entityDTO.getId());
        if(!optional.isPresent()) {
            throw new SecurityException("no such user");
        }
        @NotNull final User user = optional.get();
        updateEntity(user, entityDTO);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void persist(@NotNull final UserDTO entityDTO) {
        entityDTO.setId(UUID.randomUUID().toString());
        @NotNull final User user = new User();
        updateEntity(user, entityDTO);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String id) {
        @NotNull final Optional<User> optional = userRepository.findById(id);
        if(!optional.isPresent()) {
            throw new SecurityException("no such user");
        }
        @NotNull final User user = optional.get();
        userRepository.delete(user);
    }

    @Nullable
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return self.getUserByLogin(s);
    }

    private UserDTO produceDTO(@NotNull final User user) {
        @NotNull final UserDTO userDTO = new UserDTO();
        if(user.getId() == null) {
            throw new SecurityException("User is not valid");
        }
        userDTO.setId(user.getId());
        if(user.getLogin() == null) {
            throw new SecurityException("User is not valid");
        }
        userDTO.setLogin(user.getLogin());
        userDTO.setPassword(user.getPasswordHash() != null ? user.getPasswordHash() : "");
        userDTO.setPasswordHash(user.getPasswordHash() != null ? user.getPasswordHash() : "");
        userDTO.setRole(user.getRole() != null ? user.getRole() : Role.USER);
        userDTO.setActive(user.isActive());
        return userDTO;
    }

    private void updateEntity(
            @NotNull final User user,
            @NotNull final UserDTO userDTO
    ) {
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPasswordHash(userDTO.getPasswordHash());
        user.setRole(userDTO.getRole());
        user.setActive(userDTO.isActive());
    }
}
