package ru.evseenko.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.evseenko.api.repository.IProjectRepository;
import ru.evseenko.api.repository.ITaskRepository;
import ru.evseenko.api.repository.IUserRepository;
import ru.evseenko.api.service.ITaskService;
import ru.evseenko.entity.Project;
import ru.evseenko.entity.Task;
import ru.evseenko.entity.User;
import ru.evseenko.entity.dto.TaskDTO;
import ru.evseenko.exception.ServiceException;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService extends AbstractDomainService<TaskDTO, Task> implements ITaskService {

    private final ITaskRepository taskRepository;
    private final IProjectRepository projectRepository;
    private final IUserRepository userRepository;

    @Autowired
    public TaskService(
            ITaskRepository taskRepository,
            IProjectRepository projectRepository,
            IUserRepository userRepository
    ) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
    }

    @NotNull
    @Override
    ITaskRepository getDomainRepository() {
        return taskRepository;
    }

    @Override
    void updateEntity(Task entity, TaskDTO entityDTO) {
        @NotNull final Optional<User> optional = userRepository.findById(entityDTO.getUserId());
        if(!optional.isPresent()) {
            throw new ServiceException("No such task");
        }
        entity.setUser(optional.get());

        if (!entityDTO.getProjectId().isEmpty()) {
            @NotNull final Optional<Project> optionalProject = projectRepository.findById(entityDTO.getProjectId());
            if(!optionalProject.isPresent()) {
                throw new ServiceException("no such Project");
            }
            entity.setProject(optionalProject.get());
        }
    }

    @Override
    void updateDTO(
            @NotNull final Task entity,
            @NotNull final TaskDTO entityDTO
    ) {
        if(entity.getUser() == null) {
            throw new ServiceException("Task is not valid");
        }
        @NotNull final User user = entity.getUser();

        if(user.getId() == null) {
            throw new ServiceException("User is not valid");
        }
        entityDTO.setUserId(user.getId());
        if (entity.getProject() != null) {
            if(entity.getProject().getId() == null) {
                throw new ServiceException("Project is not valid");
            }
            entityDTO.setProjectId(entity.getProject().getId());
        }
    }

    @NotNull
    @Override
    Class<Task> getClassEntity() {
        return Task.class;
    }

    @NotNull
    @Override
    Class<TaskDTO> getClassDTO() {
        return TaskDTO.class;
    }

    @NotNull
    @Override
    @Transactional
    public List<TaskDTO> getTaskForProject(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final List<Task> entities =
                getDomainRepository().findTaskForProject(userId, projectId);

        @NotNull final List<TaskDTO> entitiesDTO = getListDTO(entities);

        return entitiesDTO;
    }
}
