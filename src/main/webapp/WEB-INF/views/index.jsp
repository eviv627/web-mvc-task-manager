<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Account</title>
</head>
<body>
    <h2>Home</h2>
    <c:if test="${pageContext.request.userPrincipal.name != null}">
    <h2>Welcome ${pageContext.request.userPrincipal.name}!</h2>
        <form id="logoutForm" method="POST" action="${contextPath}/logout">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <p><input type="submit" value="logout" /></p>
        </form>
        <form id="logoutForm" method="GET" action="${contextPath}/project/list">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <p><input type="submit" value="projects" /></p>
        </form>
        <form id="logoutForm" method="GET" action="${contextPath}/task/list">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <p><input type="submit" value="tasks" /></p>
        </form>
    </c:if>
    <c:if test="${pageContext.request.userPrincipal.name == null}">
        <form method="GET" action="${pageContext.servletContext.contextPath}/login">
            <p><input type="submit" value="login" /></p>
        </form>
        <form method="GET" action="${pageContext.servletContext.contextPath}/registration">
            <p><input type="submit" value="registration" /></p>
        </form>
    </c:if>
</body>
</html>