<%@ page language="java" pageEncoding="UTF-8" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<title>Task status page</title>
</head>
<body>
<p>
    <b>Tasks :</b>
</p>
	<table border="1">
		<tr>
			<th>Name</th>
			<th>Description</th>
			<th>Create date</th>
			<th>Start date</th>
			<th>End date</th>
    		<th>Status</th>
    		<th>Id</th>
    		<th>Project Id</th>
    		<th>Edit</th>
    		<th>Delete</th>
		</tr>
		<c:forEach items="${tasks}" var="task" varStatus="status">
		<tr>
			<td>${task.name}</td>
			<td>${task.description}</td>
			<td>${task.createDate}</td>
			<td>${task.startDate}</td>
			<td>${task.endDate}</td>
			<td>${task.status.name}</td>
			<td>${task.id}</td>
			<td>${task.projectId}</td>
			<td>
				<form method="GET" action="${pageContext.servletContext.contextPath}/task/edit">
                    <input type="hidden" name="id" value=${task.id}>
                    <p><input type="submit" value="edit" /></p>
                </form>
            </td>
            <td>
				<form method="POST" action="${pageContext.servletContext.contextPath}/task/delete">
				    <input type="hidden" name="id" value=${task.id}>
				    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <p><input type="submit" value="delete" /></p>
                </form>
			</td>
		</tr>
		</c:forEach>
		</table>
	<p>
        <a href="${pageContext.servletContext.contextPath}/task/add">
            <input type="button" value=" Add " />
        </a>
        <a href="${pageContext.servletContext.contextPath}/">
            <input type="button" value="Back to main" /
        </a>
    </p>
</body>
</html>