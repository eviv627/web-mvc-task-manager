<%@ page language="java" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
	<title>Add task</title>
</head>
<body>
<h1>Add task :</h1>
<form:form method="post" action="add" modelAttribute="task">
	<table>
	    <tr>
            <td><form:hidden  path="userId" /></td>
        </tr>
		<tr>
			<td align="right" >Task name : </td>
			<td>
				<form:input path="name" required="required"/>
			</td>
		</tr>
		<tr>
			<td align="right" >Task description : </td>
			<td>
				<form:input path="description" required="required"/>
			</td>
		</tr>
		<tr>
			<td align="right" >Task start date : </td>
			<td>
				<form:input path="startDate" pattern="${datePattern}" value="yyyy-MM-ddTHH:mm:ss+zzzz"/>
			</td>
		</tr>
		<tr>
			<td align="right" >Task end date : </td>
			<td>
				<form:input path="endDate" pattern="${datePattern}" value="yyyy-MM-ddTHH:mm:ss+zzzz"/>
			</td>
		</tr>
		<tr>
			<td align="right" >Parent project id : </td>
			<td>
				<form:input path="projectId" pattern="${projectIdPattern}"/>
			</td>
		</tr>
		<tr>
			<td align="right" >Status : </td>
			<td>
				<form:select path="status" required="required">
					<form:options items="${statuses}" />
				</form:select>
			</td>
		<tr>
		<tr>
		    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<td><input type="submit" value="Submit"/></td>
		</tr>
	</table>
</form:form>
</body>
