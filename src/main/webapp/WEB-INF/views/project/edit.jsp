<%@ page language="java" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
	<title>Edit project</title>
</head>
<body>
<h1>Edit project :</h1>
<form:form method="post" action="edit" modelAttribute="project">
	<table>
		<tr>
			<td></td>
			<td><form:hidden  path="id" /></td>
		</tr>
		<tr>
			<td></td>
			<td><form:hidden  path="userId" /></td>
		</tr>
		<tr>
			<td></td>
			<td><form:hidden  path="createDate" /></td>
		</tr>
		<tr>
			<td align="right" >Project name : </td>
			<td>
				<form:input path="name" required="required"/>
			</td>
		</tr>
		<tr>
			<td align="right" >Project description : </td>
			<td>
				<form:input path="description" required="required"/>
			</td>
		</tr>
		<tr>
			<td align="right" >Project start date : </td>
			<td>
				<c:set var="startDate" scope="session" value="yyyy-MM-dd'T'HH:mm:ss+zzzz" />
				<c:if test="${not empty project.startDate}">
					<c:set var="startDate" scope="session" value="${project.startDate}" />
				</c:if>
				<form:input path="startDate" pattern="${datePattern}" value="${startDate}"/>
			</td>
		</tr>
		<tr>
			<td align="right" >Project end date : </td>
			<td>
				<c:set var="endDate" scope="session" value="yyyy-MM-dd'T'HH:mm:ss+zzzz" />
				<c:if test="${not empty project.endDate}">
					<c:set var="endDate" scope="session" value="${project.endDate}" />
				</c:if>
				<form:input path="endDate" pattern="${datePattern}" value="${endDate}"/>
			</td>
		</tr>
		<tr>
			<td align="right" >Status : </td>
			<td>
				<form:select path="status" required="required">
					<form:options items="${statuses}" />
				</form:select>
			</td>
		<tr>
		<tr>
		    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			<td><input type="submit" value="Submit"/></td>
		</tr>
	</table>
</form:form>
</body>