<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <title>Create an account</title>
  </head>

<body>
    <form:form method="POST" modelAttribute="user">
    <h2>Create your account</h2>
    <span>${error}</span>
        <table>
            <tr>
                <td>Enter login:</td>
                <td><form:input type="text" path="login" required="required"/></td>
            </tr>
            <tr>
                <td>Enter password:</td>
                <td><form:input type="text" path="password" required="required"/></td>
            </tr>
            <tr>
                <td>Confirm password:</td>
                <td><form:input type="text" path="passwordConfirm" required="required"/></td>
            </tr>
            <tr>
                <td><input type="submit" value="Submit"></td>
            </tr>
            <tr>
        </table>
    </form:form>
</body>
</html>