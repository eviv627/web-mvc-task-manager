<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <title>Log in with your account</title>
  </head>

<body>
    <form method="POST" modelAttribute="user">
    <h2>Log in :</h2>
    <span>${error}</span>
    <span>${message}</span>
    <form method="POST" action="${contextPath}/login">
        <table>
            <tr>
                <td>Enter login:</td>
                <td><input type="text" name="username" required="required"/></td>
            </tr>
            <tr>
                <td>Enter password:</td>
                <td><input name="password" type="password"/></td>
                <td><input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/></td>
            </tr>
            <tr>
                <td><input type="hidden" name="remember-me" value="true"></td>
                <td>Remember me on this Computer</td>
            </tr>
            <tr>
                <td><input type="submit" value="Submit"></td>
            </tr>
            <tr>
        </table>
    </form>
    <h4><a href="${contextPath}/registration">Create an account</a></h4>
</body>
</html>